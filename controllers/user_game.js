/** @format */

const { UserGame } = require('../models');

module.exports = {
  getAllUserGame: (req, res) => {
    UserGame.findAll({
      attributes: ['id_user', 'username', 'email', 'password', 'createdAt', 'updatedAt'],
    })
      .then((result) => {
        if (result.length > 0) {
          res.status(200).json({ message: 'Berhasil Get All User Game', result: result });
        } else {
          res.status(404).json({ message: 'User Game Tidak di temukan', result: result });
        }
      })
      .catch((err) => {
        res.status(500).json({ message: 'Gagal Get All User Game', err: err.message });
      });
  },
  getUserGameById: (req, res) => {
    UserGame.findOne({
      where: {
        id_user: req.params.id,
      },
      attributes: ['id_user', 'username', 'email', 'password', 'createdAt', 'updatedAt'],
    })
      .then((result) => {
        if (result) {
          res.status(200).json({ message: 'Berhasil Get User Game By Id', result });
        } else {
          res.status(404).json({ message: 'User Game dengan ID ' + req.params.id + ' Tidak di temukan', result });
        }
      })
      .catch((err) => {
        res.status(500).json({ message: 'Gagal Get User Game By Id', err: err.message });
      });
  },
  createUserGame: (req, res) => {
    UserGame.create({
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
    })
      .then((result) => {
        res.status(200).json({ message: 'Berhasil Membuat User Game', result });
      })
      .catch((err) => {
        res.status(500).json({ message: 'Gagal Membuat User Game', err: err.message });
      });
  },
  updateUserGame: (req, res) => {
    UserGame.update(
      {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
      },
      {
        where: {
          id_user: req.params.id,
        },
      }
    )
      .then((result) => {
        if (result[0] === 0) {
          res.status(404).json({
            message: 'User Game dengan ID ' + req.params.id + ' Tidak di temukan',
            result,
          });
        } else {
          res.status(200).json({ message: 'Berhasil Mengupdate User Game', result });
        }
      })
      .catch((err) => {
        res.status(500).json({ message: 'Gagal Mengupdate User Game', err: err.message });
      });
  },
  deleteUserGame: (req, res) => {
    UserGame.destroy({
      where: {
        id_user: req.params.id,
      },
    })
      .then((result) => {
        if (result === 0) {
          res.status(404).json({
            message: 'User Game dengan ID ' + req.params.id + ' Tidak di temukan',
            result,
          });
        } else {
          res.status(200).json({ message: 'Berhasil Menghapus User Game', result });
        }
      })
      .catch((err) => {
        res.status(500).json({ message: 'Gagal Menghapus User Game', err: err.message });
      });
  },
};
