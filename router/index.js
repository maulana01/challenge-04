/** @format */

const router = require('express').Router();
const { getAllUserGame, getUserGameById, createUserGame, updateUserGame, deleteUserGame } = require('../controllers/user_game');

const {
  getAllUserGameHistory,
  getUserGameHistoryById,
  createUserGameHistory,
  deleteUserGameHistoryByHistoryId,
  deleteUserGameHistoryByUserGameId,
} = require('../controllers/user_game_history');

const {
  getAllUserGameBiodata,
  getUserGameBiodataById,
  createUserGameBiodata,
  updateUserGameBiodata,
  deleteUserGameBiodata,
} = require('../controllers/user_game_biodata');

// User_Games Endpoint
router.get('/get-users-games', getAllUserGame);
router.get('/get-user-games/:id', getUserGameById);
router.post('/create-user-games', createUserGame);
router.put('/update-user-games/:id', updateUserGame);
router.delete('/delete-user-games/:id', deleteUserGame);

// User_Game_Histories Endpoint
router.get('/get-user-game-histories', getAllUserGameHistory);
router.get('/get-user-game-history/:id', getUserGameHistoryById);
router.post('/create-user-game-history', createUserGameHistory);
router.delete('/delete-user-game-history-id/:id', deleteUserGameHistoryByHistoryId);
router.delete('/delete-user-game-history-usergameid/:id', deleteUserGameHistoryByUserGameId);

// User_Game_Biodatas Endpoint
router.get('/get-user-game-biodatas', getAllUserGameBiodata);
router.get('/get-user-game-biodata/:id', getUserGameBiodataById);
router.post('/create-user-game-biodata', createUserGameBiodata);
router.put('/update-user-game-biodata/:id', updateUserGameBiodata);
router.delete('/delete-user-game-biodata/:id', deleteUserGameBiodata);

module.exports = router;
